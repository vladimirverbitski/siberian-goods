'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let uglify = require('gulp-uglify');
let concat = require('gulp-concat');
let cleanCSS = require('gulp-clean-css');

let sassFiles = 'src/scss/styles.scss',
    jsFiles = [
        'src/js/jquery.min.js',
        'src/js/fullpage.js',
        'src/js/scrpits.js',
        'src/js/*.js',
    ],
    cssDest = 'src/build/',
    jsDest = 'src/build/js';

gulp.task('styles', function(){
    return gulp.src(sassFiles)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(gulp.dest(cssDest));
});

gulp.task('scripts', function () {
    return gulp.src(jsFiles)
        .pipe(uglify())
        .pipe(concat('app.js'))
        .pipe(gulp.dest(cssDest));
});

gulp.task('watch',function() {
    gulp.watch(
        sassFiles,
        gulp.series('styles')
    );
});