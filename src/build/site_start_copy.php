<!DOCTYPE html>
<html>

<head>
    <meta charset='utf-8'>
    <meta content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0' name='viewport'>
    <meta content='7bb2d3856488ff00' name='yandex-verification'>
    <insert name="show_head">

        <link href='favicon/apple-touch-icon-57x57.png' rel='apple-touch-icon' sizes='57x57'>
        <link href='favicon/apple-touch-icon-60x60.png' rel='apple-touch-icon' sizes='60x60'>
        <link href='favicon/apple-touch-icon-72x72.png' rel='apple-touch-icon' sizes='72x72'>
        <link href='favicon/apple-touch-icon-76x76.png' rel='apple-touch-icon' sizes='76x76'>
        <link href='favicon/apple-touch-icon-114x114.png' rel='apple-touch-icon' sizes='114x114'>
        <link href='favicon/apple-touch-icon-120x120.png' rel='apple-touch-icon' sizes='120x120'>
        <link href='favicon/apple-touch-icon-144x144.png' rel='apple-touch-icon' sizes='144x144'>
        <link href='favicon/apple-touch-icon-152x152.png' rel='apple-touch-icon' sizes='152x152'>
        <link href='favicon/apple-touch-icon-180x180.png' rel='apple-touch-icon' sizes='180x180'>
        <link href='favicon/favicon-32x32.png' rel='icon' sizes='32x32' type='image/png'>
        <link href='favicon/favicon-194x194.png' rel='icon' sizes='194x194' type='image/png'>
        <link href='favicon/favicon-96x96.png' rel='icon' sizes='96x96' type='image/png'>
        <link href='favicon/android-chrome-192x192.png' rel='icon' sizes='192x192' type='image/png'>
        <link href='favicon/favicon-16x16.png' rel='icon' sizes='16x16' type='image/png'>
        <link href='favicon/manifest.json' rel='manifest'>
        <meta content='#2b5797' name='msapplication-TileColor'>
        <meta content='/mstile-144x144.png' name='msapplication-TileImage'>
        <meta content='#ffffff' name='theme-color'>

        <link rel="stylesheet" media="all" href="/css/style.css" />
        <link rel="stylesheet" media="all" href="/css/sprites.css" />
        <!--<link rel="stylesheet" media="all" href="/css/home.css" />-->
        <link rel="stylesheet" media="all"
            href="https://fonts.googleapis.com/css?family=Roboto:400,500,300&amp;subset=latin,cyrillic" />
        <link rel="stylesheet" media="all"
            href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&amp;subset=latin,cyrillic" />

        <script type="text/javascript" src="/js/script.js"></script>

        <!--<script type="text/javascript" src="//yandex.st/jquery/1.11.3/jquery.min.js"></script>-->

        <link type="text/css" rel="stylesheet" href="/css/video-block.css">
        <link type="text/css" rel="stylesheet" href="/css/jquery.mb.YTPlayer.min.css" media="all">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.js"></script>
        <script src="/js/jquery.mb.YTPlayer.min.js"></script>
        <script src="/js/video.js"></script>


        <meta name="csrf-param" content="authenticity_token" />
        <meta name="csrf-token"
            content="bm+RGcMb1liC9th8nq8b/nxX33i5dknSW3VYxXmuS2VGRJ8dYuH4wzDxzp4S4OwCkHfn0/YuSptcKy4Gl3g+ZA==" />

</head>

<body>
    <div id='overlay'></div>

    <aside id='menu'>
        <ul>
            <insert name="show_block" module="menu" id="1" tag_start_1="[li]" tag_end_1="[/li]">
        </ul>
    </aside>


    <div class='container'>
        <insert name="show_include" file="header">



            <main>


                <div id="customElement">
                    <div class="content">
                        <div id="video" data-property="{
                            videoURL:'Vp80ftQMSUA', 
                            containment:'#customElement', 
                            showControls:false, 
                            mute:true, 
                            startAt:0, 
                            opacity:1, 
                            addRaster:true, 
                            quality:'default', 
                            stopMovieOnBlur:false
                            }">
                        </div>
                    </div>
                </div>

                <div class='products-wrapper transform-wrapper two-cols wrapper wrapper-of-buttons'
                    style="padding-top: 0;">
                    <div class='products-wrapper-inner'>

                        <section class="section-one">
                            <div class="section_logo"><img src="/img/section-logo.png"></div>
                        </section>

                        <div class="down-block"><img src="/img/down-home.png"></div>

                        <section class="section-two">
                            <div class="s2_bg"></div>
                            <div class="wrapper-home" style="padding-top:143px">
                                <div class="left_img_s2"><img src="/img/img1.jpg"></div>
                                <div class="right_block_s2">
                                    <insert name="show_block" module="site" id="5">
                                </div>
                            </div>
                        </section>

                        <section class="section-three">
                            <div class="wrapper-home">
                                <div class="s3-left">
                                    <insert name="show_block" module="site" id="6">
                                        <div class="tov-l"><a href="/shop/portmone/portmone-s-zamkom--viski/"
                                                class="button-home wobble-horizontal secbot3">Посмотреть</a><img
                                                src="/img/img3.jpg"></div>
                                        <div class="tov-l-1"><img src="/img/img4.png"><a
                                                href="/shop/sumki/dorozhnaya-sumka-western--konyak/"
                                                class="button-home wobble-horizontal secbot4">Посмотреть</a></div>

                                </div>
                                <div class="s3-right">
                                    <div class="img2"><img src="/img/img2.png"><a
                                            href="/shop/nesesser/nesesser--konyak/"
                                            class="button-home wobble-horizontal secbot1">Посмотреть</a></div>
                                    <div class="tov-r"><a href="/shop/ryukzaki/ryukzak-malyy--konyak/"
                                            class="button-home wobble-horizontal secbot2">Посмотреть</a><img
                                            src="/img/img5.jpg"></div>
                                    <div class="tov-r-1">
                                        <insert name="show_block" module="site" id="7">
                                    </div>
                                </div>
                            </div>
                        </section>

                        <section class="section-four">
                            <div class="s-4">
                                <insert name="show_block" module="site" id="8">
                            </div>
                        </section>

                        <section class="section-five">
                            <insert name="show_block" module="site" id="9">
                        </section>

                        <section class="section-six">
                            <div class="wrapper-home">
                                <div class="s3-left">
                                    <insert name="show_block" module="site" id="10">
                                        <div class="tov-l-s6"><a href="/shop/oblozhki-dlya-pasporta/"
                                                class="button-home wobble-horizontal secbot5">Посмотреть</a><img
                                                src="/img/img6.jpg"></div>
                                </div>
                                <div class="s3-right">
                                    <div class="tov-r-s6"><a href="/shop/portmone/portmone-s-zamkom--temnaya-oliva/"
                                            class="button-home wobble-horizontal secbot6">Посмотреть</a><img
                                            src="/img/img7.jpg"></div>
                                </div>

                        </section>

                    </div>
                </div>

                <div class='bottom-btns' id='bottom-btns'>
                    <div class='wrapper'>
                        <div class='right'>
                            <a href="/shop/" class='btn home-bot' data-action='open-filter-menu' id='filters-btn'>
                                <i>Каталог товаров</i>
                            </a>
                        </div>
                    </div>
                </div>


            </main>


            <insert name="show_include" file="footer">

    </div>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
    (function(d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter47361724 = new Ya.Metrika2({
                    id: 47361724,
                    clickmap: true,
                    trackLinks: true,
                    accurateTrackBounce: true,
                    webvisor: true
                });
            } catch (e) {}
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function() {
                n.parentNode.insertBefore(s, n);
            };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else {
            f();
        }
    })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript>
        <div><img src="https://mc.yandex.ru/watch/47361724" style="position:absolute; left:-9999px;" alt="" /></div>
    </noscript>
    <!-- /Yandex.Metrika counter -->
</body>

</html>